#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define STRINGA_MAX 20
#define LIBRI_MAX 20
#define CLIENTI_MAX 20
#define AUTORI_MAX 20
#define PRESTITI_MAX 10

typedef struct {
    char nome[STRINGA_MAX];
    char cognome[STRINGA_MAX];
    char nazionalita[STRINGA_MAX];
} autore;

typedef struct {
    char titolo[STRINGA_MAX];
    char ISBN[STRINGA_MAX];
    int num_autori;
    autore lista_autori[AUTORI_MAX];
    int num_copie;
} libro;

typedef struct {
    char nome[STRINGA_MAX];
    char cognome[STRINGA_MAX];
    int codice;
} cliente;

typedef struct {
    libro lista_libri [LIBRI_MAX];
    int num_libri;
} archivio_libri;

typedef struct {
    cliente lista_clienti [CLIENTI_MAX];
    int num_clienti;
    int codice_cliente;
} archivio_clienti;

typedef struct {
    char ISBN[STRINGA_MAX];
    int codice_cliente;
    char titolo [STRINGA_MAX];
    char nome [STRINGA_MAX];
    char cognome [STRINGA_MAX];
} prestito;

typedef struct {
    int num_prestiti;
    prestito lista_prestiti[PRESTITI_MAX];
} archivio_prestiti;

void num_libri(archivio_libri*);
void num_clienti(archivio_clienti*);
void num_prestiti(archivio_prestiti*);
libro leggi_libro();
int inserisci_libro(archivio_libri*, libro);
cliente leggi_cliente();
int inserisci_cliente(archivio_clienti*, cliente);
int ricerca_codice(archivio_clienti, int);
int ricerca_ISBN(archivio_libri, char[]);
int controllo_prestiti(archivio_prestiti, int);
void stampa_libri(archivio_libri);

int main(int argc, char** argv) {

    int scelta, aggiunta_libro, aggiunta_cliente, codice_ricerca, esito_ricerca_codice, codice_cl, esito_ricerca_ISBN;
    char ISBN[STRINGA_MAX];
    int risposta, indice_prestiti, esito, i, j;

    cliente nuovo_cliente;
    libro nuovo_libro;
    autore nuovo_autore;
    prestito nuovo_prstito;
    archivio_clienti ac;
    archivio_libri al;
    archivio_prestiti ap;

    j = 0;

    num_libri(&al);
    num_clienti(&ac);
    num_prestiti(&ap);



    do {
        system("clear");

        printf("\t\t\t\t\tBIBLIOTECA\t\t\t\t\n\n\n");
        printf("1 INSERISCI LIBRO\n");
        printf("2 TESSERAMENTO CLIENTE\n");
        printf("3 PRESTITO\n");
        printf("4 VISUALIZZA PRESTITI\n");
        printf("5 VISUALIZZA LIBRI\n");
        printf("0 USCITA\n\n");
        printf("SCELTA: ");
        scanf("%d", &scelta);

        switch (scelta) {

            default:
                printf("ERRORE, comando non valido\n");
                getchar();
                getchar();
                break;

            case 0:
                printf("ARRIVERDERCI\n\n");
                break;

            case 1:
                system("clear");
                printf("Funzione: inserisci cliente\n\n");
                nuovo_libro = leggi_libro();
                aggiunta_libro = inserisci_libro(&al, nuovo_libro);
                switch (aggiunta_libro) {
                    case 0:
                        printf("I dati sono stati inseriti\n");
                        getchar();
                        getchar();
                        break;
                    case -1:
                        printf("ERRORE, l'archivio e' pieno\n");
                        getchar();
                        getchar();
                        break;
                }
                break;

            case 2:
                system("clear");
                printf("Funzione: inserisci cliente\n\n");
                nuovo_cliente = leggi_cliente();
                aggiunta_cliente = inserisci_cliente(&ac, nuovo_cliente);
                switch (aggiunta_cliente) {
                    case 0:
                        printf("I dati sono stati inseriti\n");
                        codice_cl = ac.codice_cliente;
                        printf("Il codice e': %d\n", codice_cl);
                        ac.lista_clienti[j].codice = codice_cl;
                        ac.codice_cliente++;
                        j++;
                        getchar();
                        getchar();
                        
                        break;
                    case -1:
                        printf("ERRORE, l'archivio e' pieno\n");
                        getchar();
                        getchar();
                        break;
                }
                break;



            case 3:
                system("clear");
                printf("Funzione: prestito libro\n");
                indice_prestiti = ap.num_prestiti; //indice per inserire i prestiti (come si usa nell'archivio)
                printf("Inserisci il codice del cliente da ricercare: ");
                scanf("%d", &codice_ricerca);
                esito_ricerca_codice = ricerca_codice(ac, codice_ricerca);
                if (esito_ricerca_codice != -2) {
                    printf("I il codice e' stato trovato\n");
                    printf("Nome del cliente: %s\n", ac.lista_clienti[esito_ricerca_codice].nome);
                    printf("Cognome del cliente: %s\n", ac.lista_clienti[esito_ricerca_codice].cognome);
                    strcpy(ap.lista_prestiti[indice_prestiti].nome, ac.lista_clienti[esito_ricerca_codice].nome); //copio in archivio
                    strcpy(ap.lista_prestiti[indice_prestiti].cognome, ac.lista_clienti[esito_ricerca_codice].cognome);
                } else {

                    printf("ERRORE, il codice non e' stato trovato\n");

                }
                printf("Inserisci l'ISBN da ricercare: ");
                scanf("%s", ISBN);
                esito_ricerca_ISBN = ricerca_ISBN(al, ISBN);
                if (esito_ricerca_ISBN != -2) {
                    printf("L'ISNB e' stato trovato\n");
                    printf("Titolo: %s\n", al.lista_libri[esito_ricerca_ISBN].titolo);
                    strcpy(ap.lista_prestiti[indice_prestiti].titolo, al.lista_libri[esito_ricerca_ISBN].titolo);
                    strcpy(ap.lista_prestiti[indice_prestiti].ISBN, al.lista_libri[esito_ricerca_ISBN].ISBN);
                    printf("Confermi i dati (0 conferma - 1 annulla)\n");
                    scanf("%d", &risposta);
                    if (risposta == 0) {
                        if (al.lista_libri[esito_ricerca_ISBN].num_copie > 0) {
                            ap.num_prestiti++;
                            al.lista_libri[esito_ricerca_ISBN].num_copie--;
                            printf("Prestito ok\n");
                        } else {
                            printf("Numero di copie insufficienti\n");
                        }
                    } else {
                        printf("operazione annullata\n");
                    }
                } else {
                    printf("ERRORE, L'ISNB non e' stato trovato\n");
                }
                getchar();
                getchar();
                break;

            case 4:
                system("clear");
                printf("Funzione: stampa prestiti\n\n");
                printf("Inserisci il codice del cliente\n");
                scanf("%d", &codice_ricerca);
                esito = controllo_prestiti(ap, codice_ricerca);
                if (esito != -2) {
                    printf("Nome cliente: %s\n", ap.lista_prestiti[esito].nome);
                    printf("Cognome cliente: %s\n", ap.lista_prestiti[esito].cognome);
                    for (i = 0; i < ap.num_prestiti; i++) {
                        printf("Titolo: %s\n", ap.lista_prestiti[i].titolo);
                        printf("ISBN: %s\n", ap.lista_prestiti[i].ISBN);
                    }
                } else {
                    printf("Errore, codice non trovato\n");
                }
                getchar();
                getchar();
                break;

            case 5:
                system("clear");
                printf("Funzione: stampa libri\n\n");
                stampa_libri(al);
                getchar();
                getchar();
                break;
        }
    } while (scelta != 0);

    return (EXIT_SUCCESS);
}

void num_libri(archivio_libri* _al) {
    _al->num_libri = 0;
}

void num_clienti(archivio_clienti* _ac) {
    _ac->num_clienti = 0;
    _ac->codice_cliente = 1;
}

void num_prestiti(archivio_prestiti* _ap) {
    _ap->num_prestiti = 0;
}

libro leggi_libro() {

    int i;

    libro _nuovo_libro;

    printf("Inserisci il titolo del libro: ");
    scanf("%s", _nuovo_libro.titolo);
    printf("Inserisci l'ISBN: ");
    scanf("%s", _nuovo_libro.ISBN);
    printf("Inserisci il numero di copie: ");
    scanf("%d", &_nuovo_libro.num_copie);
    do {
        printf("Insersci il numero di autori: ");
        scanf("%d", &_nuovo_libro.num_autori);
        if (_nuovo_libro.num_autori > 10) {
            printf("Errore, un libro puo contenere massimo 10 autori\n");
        }

    } while (_nuovo_libro.num_autori > 10);
    for (i = 0; i < _nuovo_libro.num_autori; i++) {
        printf("Inserisci il nome dell'autore: ");
        scanf("%s", _nuovo_libro.lista_autori[i].nome);
        printf("Insersci il cognome dell'autore: ");
        scanf("%s", _nuovo_libro.lista_autori[i].cognome);
        printf("Insersci la nazionalita' dell'autore: ");
        scanf("%s", _nuovo_libro.lista_autori[i].nazionalita);
    }
    return _nuovo_libro;
}

int inserisci_libro(archivio_libri* _al, libro _nuovo_libro) {

    int indice;

    indice = _al->num_libri;

    if (indice >= LIBRI_MAX) {
        return -1;
    } else {
        _al->lista_libri[indice] = _nuovo_libro;
        _al->num_libri++;

        return 0;
    }
}

cliente leggi_cliente() {

    cliente _nuovo_cliente;

    printf("Inserisci il nome del cliente: ");
    scanf("%s", _nuovo_cliente.nome);
    printf("Inserisci il cognome del cliente: ");
    scanf("%s", _nuovo_cliente.cognome);

    return _nuovo_cliente;

}

int inserisci_cliente(archivio_clienti* _ac, cliente _nuovo_cliente) {

    int indice;

    indice = _ac->num_clienti;

    if (indice >= CLIENTI_MAX) {
        return -1;
    } else {
        _ac->lista_clienti[indice] = _nuovo_cliente;
        _ac->num_clienti++;
        return 0;
    }

}

int ricerca_codice(archivio_clienti _ac, int codice_ricerca) {

    int indice_ricerca;
    indice_ricerca = 0;
    while ((indice_ricerca < _ac.num_clienti)&&(_ac.lista_clienti[indice_ricerca].codice != codice_ricerca)) {
        indice_ricerca++;
    }
    if (indice_ricerca > _ac.num_clienti) {
        return -2;
    } else {
        return indice_ricerca;
    }
}

int ricerca_ISBN(archivio_libri _al, char _ISBN[]) {

    int indice_ricerca_ISBN;

    indice_ricerca_ISBN = 0;

    while ((indice_ricerca_ISBN < _al.num_libri)&&(strcmp(_ISBN, _al.lista_libri[indice_ricerca_ISBN].ISBN) != 0)) {
        indice_ricerca_ISBN++;
    }
    if (indice_ricerca_ISBN >= _al.num_libri) {
        return -2;
    } else {
        return indice_ricerca_ISBN;
    }
}

int controllo_prestiti(archivio_prestiti _ap, int _codice) {//a differenza dell'atra funzione faccio riferimento all'archivio dei prestiti
    int indice;
    indice = 0;
    while ((indice < _ap.num_prestiti)&&(_ap.lista_prestiti[indice].codice_cliente != _codice)) {
        indice++;
    }
    if (indice > _ap.num_prestiti) {
        return -2;
    } else {
        return indice;
    }

}

void stampa_libri(archivio_libri _al) {

    int i;
    if (_al.num_libri == 0) {
        printf("L'archivio dei libri e' vuoto\n");
    }
    for (i = 0; i < _al.num_libri; i++) {
        printf("Titolo: %s\n", _al.lista_libri[i].titolo);
        printf("ISBN: %s\n\n", _al.lista_libri[i].ISBN);
    }
}
